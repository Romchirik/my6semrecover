package nsu.leorita.exchanges.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import nsu.leorita.exchanges.adapters.CurrencyAdapter
import nsu.leorita.exchanges.data.services.RangeServiceImpl
import nsu.leorita.exchanges.databinding.ActivityMainBinding
import nsu.leorita.exchanges.domain.model.Currency

class MainActivity : AppCompatActivity() {
    private val rangeService = RangeServiceImpl()
    private val adapter = CurrencyAdapter()
    private var disposable: Disposable? = null

    private var _binding: ActivityMainBinding? = null
    private val binding
        get() = requireNotNull(_binding)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        disposable = rangeService.getRanges()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { t -> adapter.data = t.ranges.values as List<Currency> },
                { t -> Log.e("ranges", t.message ?: "ranges service error") })

        val layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = layoutManager
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}