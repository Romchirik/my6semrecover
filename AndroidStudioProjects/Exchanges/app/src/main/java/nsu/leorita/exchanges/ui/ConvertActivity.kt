package nsu.leorita.exchanges.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import nsu.leorita.exchanges.databinding.ActivityConvertBinding

class ConvertActivity: AppCompatActivity(   ) {
    private var _binding: ActivityConvertBinding? = null
    private val binding
        get() = requireNotNull(_binding)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityConvertBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}