package nsu.leorita.exchanges.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import nsu.leorita.exchanges.databinding.ItemCurrencyBinding
import nsu.leorita.exchanges.domain.model.Currency

class CurrencyAdapter : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {
    var data: List<Currency> = emptyList()
        @SuppressLint("NotifyDataSetChanged")
        set(newValue) {
            field = newValue
            notifyDataSetChanged()
        }

    class CurrencyViewHolder(
        val binding: ItemCurrencyBinding
    ) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCurrencyBinding.inflate(inflater, parent, false)

        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currency = data[position]

        with(holder.binding) {
            nameTextView.text = currency.name
            symbolTextView.text = currency.code
            nominalTextView.text = currency.getRange().toString()
        }
    }

    override fun getItemCount(): Int = data.size
}