#define Nx 8000
#define Ny 8000
#define ITERS 100
#define OUTPUT_FILE "./output_matrix.dat"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

void print_matrix(double *matrix, int xSize, int ySize)
{
    for (int i = 0; i < Ny; i++)
    {
        for (int j = 0; j < Nx; j++)
        {
            printf("%f ", matrix[i * xSize + j]);
        }
        printf("\n");
    }
}

double *countRho(int size_x, int size_y, double x_a, double y_a, double hx, double hy, double x_s1, double y_s1, double x_s2, double y_s2, double R)
{
    double *rho = (double *)calloc(size_x * size_y, sizeof(double));
    for (int k = 0; k < size_x * size_y; k++)
    {
        int i = k / size_x;
        int j = k % size_x;
        double xj = x_a + j * hx;
        double yi = y_a + i * hy;

        if ((xj - x_s1) * (xj - x_s1) + (yi - y_s1) * (yi - y_s1) < R * R)
        {
            rho[k] = 0.1;
        }
        else if ((xj - x_s2) * (xj - x_s2) + (yi - y_s2) * (yi - y_s2) < R * R)
        {
            rho[k] = -0.1;
        }
        else
        {
            rho[k] = 0.0;
        }
    }
    return rho;
}

double* countRhoTerms(int size_x, int size_y, double *rho) {
    double* rhoTerms = (double*) calloc(size_x * size_y, sizeof(double));
    for (int y = 1; y < size_y - 1; y++) {
        for (int x = 1; x < size_x - 1; x++) {
            rhoTerms[y * size_x + x] = 2 * rho[y * size_x + x] 
                + 0.25 * (rho[(y - 1) * size_x + x] 
                        + rho[(y + 1) * size_x + x] 
                        + rho[y * size_x + x - 1] 
                        + rho[y * size_x + x + 1]);
        }
    }
    return rhoTerms;
}

void countNextState(int size_x, int size_y, double *grid_curr, double *grid_prev, double *rhoTerms, double main_coeff, double coeff_term_1, double coeff_term_2, double coeff_term_3)
{
    for (int y = 1; y < size_y - 1; y++)
    {
        for (int x = 1; x < size_x - 1; x++)
        {
            grid_curr[y * size_x + x] = main_coeff *
                                        (coeff_term_1 * (grid_prev[y * size_x + x - 1] + grid_prev[y * size_x + x + 1]) +
                                         coeff_term_2 * (grid_prev[(y - 1) * size_x + x] + grid_prev[(y + 1) * size_x + x]) +
                                         coeff_term_3 * (grid_prev[(y - 1) * size_x + x - 1] + grid_prev[(y - 1) * size_x + x + 1] + grid_prev[(y + 1) * size_x + x - 1] + grid_prev[(y + 1) * size_x + x + 1]) +
                                         rhoTerms[y * size_x + x]);
        }
    }
    /*double max = 0;
    int max_i = 0;
    int max_j = 0;
    for (int k = 0; k < Nx * Ny; k++)
    {
        double newDif = fabs(grid_curr[k] - grid_prev[k]);
        if (newDif > max)
        {
            max = newDif;
            max_i = k;
        }
    }
    // printf("%d: %.15f\n", max_i, max);*/
}

void mainCycle(int N_t, int size_x, int size_y, double *grid_curr, double *grid_prev, double *rho, double hx_sqr_obr, double hy_sqr_obr)
{

    double main_coeff = 0.2 / (hx_sqr_obr + hy_sqr_obr);
    double coeff_term_1 = 0.5 * (5 * hx_sqr_obr - hy_sqr_obr);
    double coeff_term_2 = 0.5 * (5 * hy_sqr_obr - hx_sqr_obr);
    double coeff_term_3 = 0.25 * (hx_sqr_obr + hy_sqr_obr);
    double* rhoTerms = countRhoTerms(size_x, size_y, rho);
    for (int i = 0; i < N_t; i++)
    {
        countNextState(size_x, size_y, grid_curr, grid_prev, rhoTerms, main_coeff, coeff_term_1, coeff_term_2, coeff_term_3);
        double *tmp = grid_prev;
        grid_prev = grid_curr;
        grid_curr = tmp;
    }
}

int main()
{
    double x_a = 0.0, x_b = 4.0, y_a = 0.0, y_b = 4.0;
    double *grid_prev = (double *)calloc(Nx * Ny, sizeof(double));
    double *grid_curr = (double *)calloc(Nx * Ny, sizeof(double));
    double hx = (x_b - x_a) / (Nx - 1), hy = (y_b - y_a) / (Ny - 1);

    double x_s1 = x_a + (x_b - x_a) / 3.0;
    double y_s1 = y_a + (y_b - y_a) * 2.0 / 3.0;
    double x_s2 = x_a + (x_b - x_a) * 2.0 / 3.0;
    double y_s2 = y_a + (y_b - y_a) / 3;

    double R;
    if ((x_b - x_a) < (y_b - y_a))
    {
        R = 0.1 * (x_b - x_a);
    }
    else
    {
        R = 0.1 * (y_b - y_a);
    }
    double *rho = countRho(Nx, Ny, x_a, y_a, hx, hy, x_s1, y_s1, x_s2, y_s2, R);
    double hx_sqr_obr = 1.0 / (hx * hx);
    double hy_sqr_obr = 1.0 / (hy * hy);

    mainCycle(ITERS, Nx, Ny, grid_curr, grid_prev, rho, hx_sqr_obr, hy_sqr_obr);

    FILE *f1 = fopen(OUTPUT_FILE, "wb");
    assert(f1);
    size_t r1 = fwrite(grid_curr, sizeof grid_curr[0], Nx * Ny, f1);
    printf("wrote %zu elements out of %d requested\n", r1, Nx * Ny);
    fclose(f1);

    return 0;
}
